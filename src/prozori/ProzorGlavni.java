package prozori;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class ProzorGlavni {
	
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout(2, false));
		shell.setText("Glavni prozor");
		shell.setSize(400, 100);
		
		Label naslovLabel = new Label(shell, SWT.NONE);
		naslovLabel.setText("Izaberite jednu od ponidjenih opcija:");
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		naslovLabel.setLayoutData(gridData);

		Button unesiDrzaveBtn = new Button(shell, SWT.PUSH);
		unesiDrzaveBtn.setText("Unesi drzave");
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		unesiDrzaveBtn.setLayoutData(gridData);
		
		Button zapocniKvizBtn = new Button(shell, SWT.PUSH);
		zapocniKvizBtn.setText("Zapocni kviz");
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		zapocniKvizBtn.setLayoutData(gridData);
		
//		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

}
