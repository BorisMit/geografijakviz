package prozori;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import domenske.klase.Drzava;
import drzave.csv.CSVReader;
import drzave.csv.CSVUtils;

public class ProzorDrzava {
	
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout(2, false));
		shell.setSize(350, 300);
		shell.setText("Unos drzave");
		
		Label naslovLabel = new Label(shell, SWT.NONE);
		naslovLabel.setText("Unesite osnovne podatke o drzavi:");
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		naslovLabel.setLayoutData(gridData);

		Label nameLabel = new Label(shell, SWT.NONE);
		nameLabel.setText("Naziv:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.CENTER;
		nameLabel.setLayoutData(gridData);

		Text nameText = new Text(shell, SWT.BORDER);
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		nameText.setLayoutData(gridData);
		//			nameText.setText("Text grows horizontally");

		Label glavniGradLabel = new Label(shell, SWT.NONE);
		glavniGradLabel.setText("Glavni grad:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.CENTER;
		glavniGradLabel.setLayoutData(gridData);

		Text glavniGradText = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.CENTER;
		gridData.grabExcessVerticalSpace = true;
		glavniGradText.setLayoutData(gridData);
		//			glavniGradText.setText("This text field and the List\nbelow share any excess space.");

		Label brojStanovnikaLabel = new Label(shell, SWT.NONE);
		brojStanovnikaLabel.setText("Broj stanovnika:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.CENTER;
		brojStanovnikaLabel.setLayoutData(gridData);

		Text brojStanovnikaText = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.CENTER;
		gridData.grabExcessVerticalSpace = true;
		brojStanovnikaText.setLayoutData(gridData);
		
		Label povrsinaLabel = new Label(shell, SWT.NONE);
		povrsinaLabel.setText("Povrsina:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.CENTER;
		povrsinaLabel.setLayoutData(gridData);

		Text povrsinaText = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.CENTER;
		gridData.grabExcessVerticalSpace = true;
		povrsinaText.setLayoutData(gridData);
		
		Label kontinentLabel = new Label(shell, SWT.NONE);
		kontinentLabel.setText("Kontinent:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.CENTER;
		kontinentLabel.setLayoutData(gridData);

		Text kontinentText = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.CENTER;
		gridData.grabExcessVerticalSpace = true;
		kontinentText.setLayoutData(gridData);
		
		Label dohodakLabel = new Label(shell, SWT.NONE);
		dohodakLabel.setText("Dohodak:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.CENTER;
		dohodakLabel.setLayoutData(gridData);

		Text dohodakText = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.CENTER;
		gridData.grabExcessVerticalSpace = true;
		dohodakText.setLayoutData(gridData);
		
		Label praznaLabel = new Label(shell, SWT.NONE);
		gridData.horizontalSpan = 1;
		praznaLabel.setLayoutData(gridData);

		Button snimiButton = new Button(shell, SWT.PUSH);
		snimiButton.setText("Snimi");
		gridData = new GridData();
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.CENTER;
		gridData.grabExcessVerticalSpace = true;
		snimiButton.setLayoutData(gridData);
		
		snimiButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				try {
					boolean prviUnos = false;
					List<Drzava> ucitaneDrzave = CSVReader.ucitajDrzave();
					if (ucitaneDrzave.isEmpty()) {
						prviUnos = true;
						proveriPodatke(prviUnos);
					} else {
						proveriPodatke(prviUnos);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
			private void proveriPodatke(boolean prviUnos) throws IOException {
				String naziv = nameText.getText().trim();
				String glavniGrad = glavniGradText.getText().trim();
				String brojStanovnika = brojStanovnikaText.getText().trim();
				String povrsina = povrsinaText.getText().trim();
				String kontinent = kontinentText.getText().trim();
				String dohodak = dohodakText.getText();
				
				String csvFile = "C:/Development/drzave/drzave.csv";
				FileWriter writer;
				if (prviUnos) {
					writer = new FileWriter(csvFile);
				} else {
					writer = new FileWriter(csvFile, true);
				}
		        

		        List<Drzava> drzave = new ArrayList<Drzava>();
		        drzave.add(new Drzava(naziv, glavniGrad, Long.valueOf(brojStanovnika), Long.valueOf(povrsina), kontinent, Long.valueOf(dohodak)));

		        //for header
		        if (prviUnos) {
		        	CSVUtils.writeLine(writer, Arrays.asList("Naziv", "GlavniGrad", "BrojStanovnika", "Povrsina", "Kontinent", "Dohodak", "GustinaNaseljenosti"));
				}

		        for (Drzava d : drzave) {

		            List<String> list = new ArrayList<>();
		            list.add(d.getNaziv());
		            list.add(d.getGlavniGrad());
		            list.add(String.valueOf(d.getBrojStanovnika()));
		            list.add(String.valueOf(d.getPovrsina()));
		            list.add(d.getKontinent());
		            list.add(String.valueOf(d.getDohodak()));
		            list.add(String.valueOf(d.getGustinaNaseljenosti()));

		            CSVUtils.writeLine(writer, list);

					//try custom separator and quote.
					//CSVUtils.writeLine(writer, list, '|', '\"');
		        }

		        writer.flush();
		        writer.close();
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
			}
		});

//		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}
	
	
}
